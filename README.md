# DFL SuperSet

### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>
* Various dependencies of the subprojects - Consult their README.md for more details

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/marcusbritanicus/dfl-superset dfl-superset`
- Enter the `dfl-superset` folder
  * `cd dfl-superset`
- Update submodules
  * `git submodule update --init --recursive`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`
